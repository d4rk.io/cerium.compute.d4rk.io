#!/bin/env bash
set -e

function help {
  echo "usage:"  
  echo "  $0 < -l | -s | -h > [ apps ... ]"
  echo ""
  echo "-l: load .env´s"
  echo "-s: store .env´s"
  echo "-h: show this page"
}

function store {
  if [ -d .env ]; then
    echo "$0: storing env for: ${APPS}"
    
    for app in ${APPS}; do
      if [ -f ${app}/.env ]; then
        sudo cp ${app}/.env .env/${app}.env
        sudo ccrypt -e -f -k .env-key .env/${app}.env
      else
        echo "$0: ${app}/.env dose not exist!"
      fi
    done

    echo "$0: You need to manualy commit the store envs!"
  fi
}

function load {
  if [ -d .env ]; then
    echo "$0: loading env for: ${APPS}"
    
    for app in ${APPS}; do
      if [ -f .env/${app}.env.cpt ]; then
        sudo cp .env/${app}.env.cpt ${app}/.env.cpt
        sudo ccrypt -d -f -k .env-key ${app}/.env.cpt
      else
        echo "$0: .env/${app}.env dose not exist!"
      fi
    done
  fi
}

APPS="ingress-traefik-r1-3463d0fee9457fb4 workadventure-r1-30cd1159114e68f3"

while getopts "slh" arg; do
  case "${arg}" in
    s) 
      if [ "${cmd}" == "" ]; then
        cmd=store
      else
        echo "$0: store (s) and load (l) are mutual exclusive!"
        exit 1;
      fi
      ;;
    l)
      if [ "${cmd}" == "" ]; then
        cmd=load
      else
        echo "$0: store (s) and load (l) are mutual exclusive!"
        exit 1;
      fi
      ;;
    h)
      help
      exit; 
      ;;
  esac
done
shift $(($OPTIND - 1))

if [ $# -gt 0 ]; then
  APPS="$@"
fi

case "${cmd}" in
  store) store;;
  load) load;;
  *) help;;
esac
