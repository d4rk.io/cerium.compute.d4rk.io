# cerium.compute.d4rk.io

## 2021 refactor
- new git repos (again)
- use traefik instead of docker-nignx proxy with letsencrypt auto gen
- use uuids for collision evasion, instead of domains - no i dont want to use docker stack - its ipv6 support sucks
- make domains now configurable
- dont support auto deply from gitlab (at least for now)
- system specific config and secrets via .env fieles
- system config is definde by an gitrepo (srv/etc/)
  - apps are includes as submudules with uuid in there name (docker compose naming)
  - env are store encrypted in an special repot. it is also a submodule
- [ ] complet rework for all cerium apps
- [ ] move mastondon and riot to cerium
- added workadvenure

## future reworks?
- use nix for system config ( or not if it is only a hassle with docker ) => probably another rework
- no kubernets for now - if i start using it i might also switch hear => rework

